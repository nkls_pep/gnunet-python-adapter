#Examples for the Python GNUnet REST Connector

import gnunet


#make instance of connector, 
#without arguments it defaults to localhost, port 7776
print("\nMaking instance of connector")
conn=gnunet.GNUnetConnector()
#it is also possible to specify host and port, for example:
#conn=gnunet.GNUnetConnector("192.168.1.77","7775")

#test if connection is possible
print("\nTesting Connector")
if(conn.test()==True):
	print("Connection established")
else:
	print("Connection not possible")
	quit()

#IDENTITIES#
#==========#

#first we create an identity named alice.
#the function createIdentity returns a boolean for success or failure.
print("\nCreating some identities")
idname="alice"
success=conn.createIdentity(idname)
if(success==True):
	print("Identity ",idname," created")
else:
	print("Error: Identity not created")

#then we create two more identities, bob and carol. 
idname="bob"
success=conn.createIdentity(idname)
if(success==True):
	print("Identity ",idname," created")
else:
	print("Error: Identity not created")

idname="carol"
success=conn.createIdentity(idname)
if(success==True):
	print("Identity ",idname," created")
else:
	print("Error: Identity not created")



#now we retrieve a list of all existing identities.
#the function returns a list of dictionaries.
print("\nRetrieving identities")
ids=conn.getIdentityList()
for x in ids:
	print("Name: ",x["name"],"\nPublic Key: ",x["pubkey"],"\n")


#to retrieve the public key of specific identity, we use this:
pubkey_alice=conn.getIdentityPubkey("alice")
print("Public key of alice: ",pubkey_alice)

#to get the identity name corresponding to a public key, we use this:
name_alice=conn.getIdentityName(pubkey_alice)
print("Name for public key ",pubkey_alice, ": ",name_alice)

#next we change the name of identity "bob" to "dorothy".
print("\Changing name of identity")
from_id="bob"
to_id="dorothy"
success=conn.changeIdentityName("bob","dorothy")
if(success==True):
	print("Changed identity ",from_id," to ",to_id)
else:
	print("Error: Identity not changed")

#in the end, we delete all created identities.
print("\nDeleting identities")
if(conn.deleteIdentity("alice")):print("deleted alice")
if(conn.deleteIdentity("dorothy")):print("deleted dorothy")
if(conn.deleteIdentity("carol")):print("deleted carol")	


#NAMESTORE#
#=========#

#to use the namestore subsystem, we first need an identity (also called 'ego')
print("\nCreating identity for namestore")
idname="emily"
success=conn.createIdentity(idname)
if(success==True):
	print("Identity ",idname," created")
else:
	print("Error: Identity not created")

#then, we have to make the created identity the default ego of namestore subsystem.
#note: the name of that identity (or ego) is also the name of the zone
print("\nMake identity the namestore default ego")
success=conn.setNamestoreIdentity(idname)
if(success==True):
	print(idname," is now the namestore default identity.")
else:
	print("Setting namestore default identity failed")

#now we add some namestore entries to the zone.
#syntax is: addNamestoreEntry(zone,name,value,type)
print("\nAdding some namestore entries")
zonename=idname
success1=conn.addNamestoreEntry(zonename,"google","8.8.8.8","A")
success2=conn.addNamestoreEntry(zonename,"pepfoundation","95.128.36.146","A")
success3=conn.addNamestoreEntry(zonename,"gnunet","131.159.74.67","A")
if(success1 and success2 and success3):
	print("3 records created")
else:
	print("failure creating records")


#we can change existing namestore entries.
print("\nChanging namestore entry")
success=conn.changeNamestoreEntry(zonename,"google","6.6.6.6","A")
if(success):
	print("Namestore entry changed")
else:
	print("failure changing namestore entry")


#now we show all existing namestore entries.
print("\nRetrieving namestore entries")
entries=conn.getNamestoreEntries(zonename)
for x in entries:
	print("\n")
	print("Name: ",x["record_name"])
	for y in x["data"]:
		print("Value: ",y["value"],"   Type:",y["record_type"])

#delete all the namestore entries
print("\nDeleting all namestore entries")
success1=conn.deleteNamestoreEntry(zonename,"google")
success2=conn.deleteNamestoreEntry(zonename,"pepfoundation")
success3=conn.deleteNamestoreEntry(zonename,"gnunet")
if(success1 and success2 and success2):
	print("Namestore entries successfully deleted")
else:
	print("Error deleting namestore entries")


#GNS#
#===#

#first we create 3 namestore entries.
print("\nCreating namestore entries")
zonename=idname
success1=conn.addNamestoreEntry(zonename,"testrecord","95.128.36.146","A")
success2=conn.addNamestoreEntry(zonename,"testrecord","Notes for the record","TXT")
success3=conn.addNamestoreEntry(zonename,"testrecord","Further notes for the record","TXT")
success3=True
if(success1 and success2 and success3):
	print("3 records created")
else:
	print("failure creating records")

#then we retrieve entries for the test record
print("\nRetrieving GNS records by name")
entries=conn.getGNSValuesOfName(zonename,"testrecord")
print("\n")
print("Record name: ",entries["record_name"])
for x in entries["data"]:
	print("Type: ",x["record_type"],"   Value: ",x["value"])

#now we only retrieve entries for the test of type "TXT"
print("\nRetrieving GNS records by name and type")
entries=conn.getGNSValuesOfNameAndType(zonename,"testrecord","TXT")
print("\n")
print("Record name: ",entries["record_name"])
for x in entries["data"]:
	print("Type: ",x["record_type"],"   Value: ",x["value"])


#in the end, we delete the test record.
print("\nDeleting test record")
success=conn.deleteNamestoreEntry(zonename,"testrecord")
if(success):
	print("Namestore entry successfully deleted")
else:
	print("Error deleting namestore entries")
