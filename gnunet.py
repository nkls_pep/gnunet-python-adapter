import requests
import json
import socket

class GNUnetConnector:
	def __init__(self,host="localhost",port="7776"):
		self.host=host
		self.port=port

	def test(self):
		#check if port is open
		#not the best solution, it should better check if REST responds \
		#and not only if port is listening
		testsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		testlocation = (self.host, int(self.port))
		testresult = testsocket.connect_ex(testlocation)
		testsocket.close()
		if testresult == 0:
			return True
		else:
			return False


# Identity Section
	def getIdentityList(self):
		#returns a list of all identities
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/"
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val)

	def getIdentityPubkey(self,thename):
		#returns the public key of an identity specified by name
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/name/"+thename
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val["pubkey"])
	def getIdentityName(self,thepubkey):
		#returns the name of an identity specified by public key
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/pubkey/"+thepubkey
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val["name"])
	
	def getNamestoreIdentity(self):
		#returns the default identity of subsystem 'namestore'
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/subsystem/namestore"
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val["name"])
	
	def createIdentity(self,thename):
		#creates Identy, returns True if successful, False if not
		requeststring="http://"+self.host+":" \
		+self.port+"/identity"
		datadict={"name": thename}
		dataparams=json.dumps(datadict)
		respdata=requests.post(requeststring,data=dataparams)
		if("201" in str(respdata)):
			#successful
			return True
		else:
			#not successful
			return False
		return()

	def changeIdentityName(self,thename,newname):
		#changes name of identity, returns True if successful, False if not
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/name/"+thename
		datadict={"newname": newname}
		dataparams=json.dumps(datadict)
		respdata=requests.put(requeststring,data=dataparams)
		if("204" in str(respdata)):
			#successful
			return True
		else:
			return False

	def deleteIdentity(self,thename):
		#deletes Identity, returns True if successful, false if not
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/name/"+thename
		respdata=requests.delete(requeststring)
		if("204" in str(respdata)):
			#successful
			return True
		else:
			return False

# Namestore Section

	def setNamestoreIdentity(self,thename):
		#sets the default Identity for subsystem 'namestore'
		#returns True if successful False if not
		requeststring="http://"+self.host+":" \
		+self.port+"/identity/subsystem/"+thename
		datadict={"subsystem": "namestore"}
		dataparams=json.dumps(datadict)
		respdata=requests.put(requeststring,data=dataparams)
		if("204" in str(respdata)):
			return True
		else:
			return False

	def getNamestoreEntries(self,thezone):
		#returns a list of all namestore entries
		requeststring="http://"+self.host+":" \
		+self.port+"/namestore/"+thezone
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val)
		#return(respdata)

	def addNamestoreEntry(self,thezone,thename,thevalue,thetype):
		#adds a namestore entry, returns true if successful, false if not
		requeststring="http://"+self.host+":" \
		+self.port+"/namestore/"+thezone
		dataparams="{\"data\": [{\"value\": \""+thevalue+"\", \"record_type\": \""+thetype+"\", \"expiration_time\":\"1d\", \"private\": false, \"relative_expiration\": false, \"supplemental\": false, \"shadow\": false}], \"record_name\": \""+thename+"\"}"
		respdata=requests.post(requeststring,data=dataparams)
		if("204" in str(respdata)):
			return True
		else:
			return False
	def changeNamestoreEntry(self,thezone,thename,thevalue,thetype):
		#changes a namestore entry, returns true if successful, false if not
		requeststring="http://"+self.host+":" \
		+self.port+"/namestore/"+thezone
		dataparams="{\"data\": [{\"value\": \""+thevalue+"\", \"record_type\": \""+thetype+"\", \"expiration_time\":\"1d\", \"private\": false, \"relative_expiration\": false, \"supplemental\": false, \"shadow\": false}], \"record_name\": \""+thename+"\"}"
		respdata=requests.put(requeststring,data=dataparams)
		if("204" in str(respdata)):
			return True
		else:
			return False

	def deleteNamestoreEntry(self,thezone,thename):
		#deletes a namestore entry, returns true if successful, false if not
		requeststring="http://"+self.host+":" \
		+self.port+"/namestore/"+thezone+"/"+thename
		respdata=requests.delete(requeststring)
		if("204" in str(respdata)):
			return True
		else:
			return False


#GNS section
	def getGNSValuesOfName(self,thezone,thename):
		#returns a list of values and record types with the same record name
		requeststring="http://"+self.host+":" \
		+self.port+"/gns/"+thename+"."+thezone
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val)

	def getGNSValuesOfNameAndType(self,thezone,thename,thetype):
		#returns a list of values of a certain name and record type
		requeststring="http://"+self.host+":" \
		+self.port+"/gns/"+thename+"."+thezone+"?record_type="+thetype
		respdata=requests.get(requeststring).json()
		val=json.loads(json.dumps(respdata))
		return(val)
